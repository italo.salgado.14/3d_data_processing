# Point Cloud 3D Repo with open3D

This repo uses open3D to work with the cloud points captured with ZED camera. There have a lot of examples to work with point clouds and operate over them.

## Installation

Use the requirements.txt


## Contributing

Italo Salgado

## License

[MIT](https://choosealicense.com/licenses/mit/)
